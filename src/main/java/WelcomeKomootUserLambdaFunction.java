import java.util.*;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.util.List;
import java.util.stream.Collectors;

public class WelcomeKomootUserLambdaFunction implements RequestHandler<SNSEvent, Void> {

    public static final String NOTIFICATION_ENDPOINT = "https://notification-backend-challenge.main.komoot.net";
    public static final String SENDER_EMAIL = "patrick.sippl@t-online.de";

    private static List<NewUser> threeRecentUsers;

    private Gson gson;
    private HttpClient httpClient;

    public WelcomeKomootUserLambdaFunction() {
        this.gson = new Gson();
        this.httpClient = HttpClients.createDefault();
        threeRecentUsers = new ArrayList<>();
    }

    public WelcomeKomootUserLambdaFunction(HttpClient httpClient) {
        this.gson = new Gson();
        this.httpClient = httpClient;
        threeRecentUsers = new ArrayList<>();
    }

    public Void handleRequest(SNSEvent event, Context context){

        List<NewUser> newUsers = getNewUsersFromSNSEvent(event);

        for(NewUser currentNewUser : newUsers) {
            this.sendNotification(currentNewUser, context);
            this.saveNewUser(currentNewUser);
        }

        return null;
    }

    private List<NewUser> getNewUsersFromSNSEvent(SNSEvent event) {

        List<NewUser> newUsers = new ArrayList<>();

        if(event.getRecords() != null) {

            for(SNSEvent.SNSRecord snsRecord : event.getRecords()) {
                SNSEvent.SNS recordSNS = snsRecord.getSNS();
                NewUser newUser = this.gson.fromJson(recordSNS.getMessage(), NewUser.class);
                newUsers.add(newUser);
            }
        }

        return newUsers;
    }

    private void saveNewUser(NewUser newUser) {

        if(threeRecentUsers.size() == 3) {
            threeRecentUsers.remove(0);
        }
        threeRecentUsers.add(newUser);
    }

    private void sendNotification(NewUser receiver, Context context) {

        String notificationMessage = this.getNotificationMessage(receiver, threeRecentUsers);
        List<Long> recentUsersIds = threeRecentUsers.stream().map(NewUser::getId).collect(Collectors.toList());

        Notification notification = Notification.builder()
                .sender(SENDER_EMAIL)
                .receiver(receiver.getId())
                .message(notificationMessage)
                .recent_user_ids(recentUsersIds)
                .build();

        String messageBody = this.gson.toJson(notification);

        try {
            HttpPost httpPost = new HttpPost(NOTIFICATION_ENDPOINT);

            StringEntity entity = new StringEntity(messageBody);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse response = this.httpClient.execute(httpPost);

            if(response.getStatusLine().getStatusCode() != 200) {
                this.logMessage("Received error from notification-service with status-code: " + response.getStatusLine().getStatusCode(), context);
            } else {
                this.logMessage("Successfully sent notification.", context);
            }

        } catch (Exception e) {
            this.logMessage("Unexpected exception occured on calling notification-service. Exception: " + e.getMessage(), context);
        }
    }

    private String getNotificationMessage(NewUser receiver, List<NewUser> otherNewUsers) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Hi ");
        stringBuilder.append(receiver.getName());
        stringBuilder.append(". welcome to komoot.");

        if(otherNewUsers.size() == 1) {
            stringBuilder.append(" ");
            stringBuilder.append(otherNewUsers.get(0).getName());
            stringBuilder.append(" also joined recently.");
        } else if(otherNewUsers.size() == 2) {
            stringBuilder.append(" ");
            stringBuilder.append(otherNewUsers.get(0).getName());
            stringBuilder.append(" and ");
            stringBuilder.append(otherNewUsers.get(1).getName());
            stringBuilder.append(" also joined recently.");
        } else if(otherNewUsers.size() == 3) {
            stringBuilder.append(" ");
            stringBuilder.append(otherNewUsers.get(0).getName());
            stringBuilder.append(", ");
            stringBuilder.append(otherNewUsers.get(1).getName());
            stringBuilder.append(" and ");
            stringBuilder.append(otherNewUsers.get(2).getName());
            stringBuilder.append(" also joined recently.");
        }

        return stringBuilder.toString();
    }

    private void logMessage(String message, Context context) {
        LambdaLogger logger = context.getLogger();
        logger.log(message);
    }
}
