import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Notification {
    private String sender;
    private Long receiver;
    private String message;
    private List<Long> recent_user_ids;
}
