import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicStatusLine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

class WelcomeKomootUserLambdaFunctionTest {

    private WelcomeKomootUserLambdaFunction objectUnderTest;

    private HttpClient httpClient;
    private HttpResponse httpResponse;

    private Context context;
    private LambdaLogger lambdaLogger;

    private final String expectedNotificationServiceUri = "https://notification-backend-challenge.main.komoot.net";

    @BeforeEach
    public void init() throws IOException {

        this.httpClient = mock(HttpClient.class);

        this.httpResponse = mock(HttpResponse.class);
        when(this.httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK!"));
        when(this.httpClient.execute(any())).thenReturn(httpResponse);

        this.context = mock(Context.class);
        this.lambdaLogger = mock(LambdaLogger.class);
        when(this.context.getLogger()).thenReturn(this.lambdaLogger);
        doNothing().when(this.lambdaLogger).log(any());

        this.objectUnderTest = new WelcomeKomootUserLambdaFunction(this.httpClient);
    }

    @Test
    void testHandleRequest_firstSNSEvent_shouldSendNotificationWithoutAnyOtherRecentlySignedUsersAndLogSuccess() throws IOException {

        this.sendSNSEvent("Mathew", 1111L);

        this.assertNotificationPostRequest(1, "{\"sender\":\"patrick.sippl@t-online.de\",\"receiver\":1111,\"message\":\"Hi Mathew. welcome to komoot.\",\"recent_user_ids\":[]}");

        verify(this.lambdaLogger, times(1)).log("Successfully sent notification.");
    }

    @Test
    void testHandleRequest_secondSNSEvent_shouldSendNotificationWitOneOtherRecentlySignedUsers() throws IOException {

        this.sendSNSEvent("Mathew", 1111L);
        this.sendSNSEvent("Magnus", 2222L);

        this.assertNotificationPostRequest(2, "{\"sender\":\"patrick.sippl@t-online.de\",\"receiver\":2222,\"message\":\"Hi Magnus. welcome to komoot. Mathew also joined recently.\",\"recent_user_ids\":[1111]}");
    }

    @Test
    void testHandleRequest_thirdSNSEvent_shouldSendNotificationWithTwoOtherRecentlySignedUsers() throws IOException {

        this.sendSNSEvent("Mathew", 1111L);
        this.sendSNSEvent("Magnus", 2222L);
        this.sendSNSEvent("Melvin", 3333L);

        this.assertNotificationPostRequest(3, "{\"sender\":\"patrick.sippl@t-online.de\",\"receiver\":3333,\"message\":\"Hi Melvin. welcome to komoot. Mathew and Magnus also joined recently.\",\"recent_user_ids\":[1111,2222]}");
    }

    @Test
    void testHandleRequest_fourthSNSEvent_shouldSendNotificationWithThreeOtherRecentlySignedUsers() throws IOException {

        this.sendSNSEvent("Mathew", 1111L);
        this.sendSNSEvent("Magnus", 2222L);
        this.sendSNSEvent("Melvin", 3333L);
        this.sendSNSEvent("Moritz", 4444L);

        this.assertNotificationPostRequest(4, "{\"sender\":\"patrick.sippl@t-online.de\",\"receiver\":4444,\"message\":\"Hi Moritz. welcome to komoot. Mathew, Magnus and Melvin also joined recently.\",\"recent_user_ids\":[1111,2222,3333]}");
    }

    @Test
    void testHandleRequest_fifthSNSEvent_shouldSendNotificationWithThreeLatestSignedUsers() throws IOException {

        this.sendSNSEvent("Mathew", 1111L);
        this.sendSNSEvent("Magnus", 2222L);
        this.sendSNSEvent("Melvin", 3333L);
        this.sendSNSEvent("Moritz", 4444L);
        this.sendSNSEvent("Maurice", 5555L);

        this.assertNotificationPostRequest(5, "{\"sender\":\"patrick.sippl@t-online.de\",\"receiver\":5555,\"message\":\"Hi Maurice. welcome to komoot. Magnus, Melvin and Moritz also joined recently.\",\"recent_user_ids\":[2222,3333,4444]}");
    }

    @Test
    void testHandleRequest_notificationServiceReturnsServerError_shouldLogErrorMessage() {

        when(this.httpResponse.getStatusLine()).thenReturn(new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_INTERNAL_SERVER_ERROR, "SERVER-ERROR!"));

        this.sendSNSEvent("Mathew", 1111L);

        verify(this.lambdaLogger, times(1)).log("Received error from notification-service with status-code: 500");
    }

    @Test
    void testHandleRequest_httpClientIOException_shouldLogErrorMessage() throws IOException {

        when(this.httpClient.execute(any())).thenThrow(new IOException("io-exception error-message"));

        this.sendSNSEvent("Mathew", 1111L);

        verify(this.lambdaLogger, times(1)).log("Unexpected exception occured on calling notification-service. Exception: io-exception error-message");
    }

    private void sendSNSEvent(String newUserName, Long newUserId) {

        SNSEvent snsEvent = this.getSNSEvent(newUserName, newUserId);
        this.objectUnderTest.handleRequest(snsEvent, this.context);
    }

    private void assertNotificationPostRequest(int expectedCalls, String expectedMessage) throws IOException {

        ArgumentCaptor<HttpPost> httpRequestArgumentCaptor = ArgumentCaptor.forClass(HttpPost.class);

        verify(this.httpClient, times(expectedCalls)).execute(httpRequestArgumentCaptor.capture());

        HttpPost actualHttpPost = httpRequestArgumentCaptor.getAllValues().get(httpRequestArgumentCaptor.getAllValues().size() - 1);
        assertThat(actualHttpPost.getURI().toString()).isEqualTo(this.expectedNotificationServiceUri);
        assertThat(actualHttpPost.getFirstHeader("content-type").toString()).isEqualTo("Content-type: application/json");
        assertThat(new String(actualHttpPost.getEntity().getContent().readAllBytes())).isEqualTo(expectedMessage);

    }

    private SNSEvent getSNSEvent(String newUserName, Long newUserId) {

        String message = "{\"created_at\": \"2026-10-29T08:11:22\",\"name\": " + newUserName + ",\"id\": " + newUserId + "}";

        SNSEvent snsEvent = new SNSEvent();
        List<SNSEvent.SNSRecord> snsRecordList = new ArrayList<>();
        SNSEvent.SNSRecord newRecord = new SNSEvent.SNSRecord();
        SNSEvent.SNS sns = new SNSEvent.SNS();
        sns.setMessage(message);
        newRecord.setSns(sns);
        snsRecordList.add(newRecord);
        snsEvent.setRecords(snsRecordList);

        return snsEvent;
    }
}